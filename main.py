import moje_funkcje as mf


def main():
    numbers = mf.get_user_numbers() # = [1,2,3,4,5,6,7,8,9,10]
    nasze_liczby_parzyste = mf.get_even_numbers(numbers)

    if nasze_liczby_parzyste[-1] < 15:
        mf.print_fibonacci_sequence(nasze_liczby_parzyste[-1])
    else:
        mf.fizz_buzz(nasze_liczby_parzyste[-1])

main()
