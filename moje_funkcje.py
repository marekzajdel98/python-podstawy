def fizz_buzz(n):
    for i in range(1, n):
        print(i)
        if i % 3 == 0:
            print('Fizz')
        if i % 5 == 0:
            print('Buzz')
        if i % 3 == 0 and i % 5 == 0:
            print('FizzBuzz')
    

def print_fibonacci_sequence(n):
    i = 0
    first_value = 0
    second_value = 1
    while(i < n):
        if(i <= 1):
            Next = i
        else:
            Next = first_value + second_value
            first_value = second_value
            second_value = Next
        print(Next)
        i = i + 1
    
def get_even_numbers(numbers):
    parzyste = []
    for i in numbers:
        if i % 2 == 0:
            parzyste.append(i)
    return(parzyste)

def get_user_numbers():
    liczby_uzytkownika = []
    for i in range(1, 11):
        try:
            liczba = int(input('Podaj liczbę'))
        except:
            print("Podana wartość musi być liczbą")
            return []
        liczby_uzytkownika.append(liczba)
    return(liczby_uzytkownika)


