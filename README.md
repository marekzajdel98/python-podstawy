# Python-Podstawy

## Zadanie zaliczeniowe nr 1 - podstawy pythona.
## Na wykonanie zadania masz 1 godzinę.
## Masz do zdobycia 20 punktów. Zadanie będzie ocenionę pod względem: ogólnego działania, złożoności, czytelności kodu i dokumentacji.

### Zadanie
## Napisz program, który składa się z 2 plików (modułów) -  pierwszy plik (moje_funkcje.py) będzie zawierał definicje potrzebnych nam funkcji, których potem użyjemy w głównym programie (main.py)

# Funkcje, które musimy zdefiniować: 
- fizz_buzz(n) (funkcja wykonująca fizz buzz dla liczb z przedziału [1, ..., n] 
- print_fibonacci_sequence(n) (funkcja wypisujaca n elementów ciągu Fibonacciego). 
  Funkcja ta będzie przyjmowała jako parametr ilość elementów do wypisania), możesz poszukać tej implementacji w internecie i postarać się zrozumieć kod.
- get_even_numbers(numbers) (funkcja przyjmująca jako parametr listę nums - równą na przykład [1,2,3,4,5] i zwracająca nową listę z wyciągniętymi liczbami parzystymi
- get_user_numbers() (funkcja która zapyta użytkownika o 10 liczb całkowitych i potem je zwróci)
 
  
# W głównym pliku zdefiniujemy i wywołamy funkcję główną main(), która nie przyjmuje żadnych parametrów.
# Kroki do wykonania w funkcji main:
	- przy uzyciu funkcji get_user_numbers() zdobadz liste 10 liczb calkowitych od uzytkownika
	- przy uzyciu funkcji get_even_numbers() wyciagnij z niej liczby parzyste - wynik zapisz do zmiennej "nasze_liczby_parzyste"
	- jezeli ostatnia liczba z naszej listy liczb parzystych (n) jest mniejsza niż 15, to wywołaj funkcję fibonacci_sequence(n) - ma wypisać n elementów ciągu Fibonacciego
	- jeżeli ostatnia liczba z naszej listy liczb parzystych (n) jest większa niż 15, to wywołaj funkcję fizz_buzz(n) - ma wykonać fizz buzz dla listy [1... n]
	